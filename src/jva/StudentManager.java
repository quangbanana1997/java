package jva;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class StudentManager {
	public static Scanner scanner = new Scanner(System.in);
	private List<Student> listStudent;
	private StudentDAO studentDAO;

	public StudentManager() throws ClassNotFoundException, IOException {
		studentDAO = new StudentDAO();
		listStudent = studentDAO.read();
	}

	public List<Student> getListStudent() {
		return listStudent;
	}

	public void setListStudent(List<Student> listStudent) {
		this.listStudent = listStudent;
	}

	public void addStudent() throws IOException {
		int id = (listStudent.size() > 0) ? (listStudent.size() + 1) : 1;
		System.out.println("Student ID: " + id);
		String name = inputName();
		byte age = inputAge();
		String address = inputAddress();
		float gpa = inputGPA();
		Student student = new Student(id, name, age, address, gpa);
		listStudent.add(student);
		studentDAO.write(listStudent);
	}

	public void updateStudent(int id) throws IOException {
		boolean isExited = false;
		for (int i = 0; i < listStudent.size(); i++) {
			if (listStudent.get(i).getId() == id) {
				isExited = true;
				listStudent.get(i).setName(inputName());
				listStudent.get(i).setAge(inputAge());
				listStudent.get(i).setAddress(inputAddress());
				listStudent.get(i).setGpa(inputGPA());
				break;
			}
		}
		if (!isExited) {
			System.out.println("id khong ton tai");
		} else {
			studentDAO.write(listStudent);
		}
	}

	public void deleteStudent(int id) throws IOException {
		Student student = null;
		for (int i = 0; i < listStudent.size(); i++) {
			if (listStudent.get(i).getId() == id) {
				student = listStudent.get(i);
				break;
			}
		}

		if (student != null) {
			listStudent.remove(student);
			studentDAO.write(listStudent);
		} else {
			System.out.println("id khong ton tai");
		}
	}

	public void sortStudentByGPA() {
		Collections.sort(listStudent, new SortStudentByGPA());
	}

	public void sortStudentByName() {
		Collections.sort(listStudent, new SortStudentByName());
	}

	public void show() {
		for (Student student : listStudent) {
			System.out.format("%5d | ", student.getId());
			System.out.format("%20s | ", student.getName());
			System.out.format("%5d | ", student.getAge());
			System.out.format("%20s | ", student.getAddress());
			System.out.format("%10.1f%n", student.getGpa());
		}
	}

	public int inputID() {
		System.out.print("Input ID: ");
		while (true) {
			try {
				int id = Integer.parseInt((scanner.nextLine()));
				return id;
			} catch (NumberFormatException ex) {
				System.out.print("invalid! Input student id again: ");
			}
		}
	}

	public String inputName() {
		System.out.print("Input name: ");
		return scanner.nextLine();
	}

	public byte inputAge() {
		System.out.print("Input age: ");
		while (true) {
			try {
				byte age = Byte.parseByte((scanner.nextLine()));
				if (age < 0 && age > 100) {
					throw new NumberFormatException();
				}
				return age;
			} catch (NumberFormatException ex) {
				System.out.print("invalid! Input student age again: ");
			}
		}
	}

	public String inputAddress() {
		System.out.print("Input address: ");
		return scanner.nextLine();
	}

	public float inputGPA() {
		System.out.print("Input gpa: ");
		while (true) {
			try {
				float gpa = Float.parseFloat((scanner.nextLine()));
				if (gpa < 0.0 && gpa > 10.0) {
					throw new NumberFormatException();
				}
				return gpa;
			} catch (NumberFormatException ex) {
				System.out.print("invalid! Input student gpa again: ");
			}
		}
	}
}