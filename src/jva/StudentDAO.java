package jva;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {

	public void write(List<Student> listStudent) {

		File file = new File("student.txt");
		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream fileOutputStream = new FileOutputStream(file);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(listStudent);

			objectOutputStream.close();
			fileOutputStream.close();
		} catch (Exception e) {
			System.out.println("loi" + e);
		}
	}

	public List<Student> read() {
		File file = new File("student.txt");
		List<Student> listStudent = new ArrayList<Student>();

		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			FileInputStream fileInputStream = new FileInputStream(file);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			listStudent = (List<Student>) objectInputStream.readObject();

			fileInputStream.close();
			objectInputStream.close();
		} catch (Exception e) {
			System.out.println("loi" + e);
		}
		
		return listStudent;
	}
}
