/*Viet chuong trinh quan ly sinh vien. Moi doi tuong sinh vien co cac thuoc tinh sau: id, name, age, address va gpa(diem trung binh). Tao ra
mot menu voi cac chuc nang sau:
	1. Add student
	2. Update student by id
	3. Delete student by id
	4. Sort student by gpa
	5. Sort student by name
	6. Show student
	0. Exit*/
package jva;

import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		String choose = null;
		boolean exit = false;
		StudentManager studentManager = new StudentManager();
		int studentID;

		showMenu();
		while (true) {
			choose = scanner.nextLine();
			switch (choose) {
			case "1":
				studentManager.addStudent();
				break;
			case "2":
				studentID = studentManager.inputID();
				studentManager.updateStudent(studentID);
				break;
			case "3":
				studentID = studentManager.inputID();
				studentManager.deleteStudent(studentID);
				break;
			case "4":
				studentManager.sortStudentByGPA();
				break;
			case "5":
				studentManager.sortStudentByName();
				break;
			case "6":
				studentManager.show();
				break;
			default:
				System.out.println("Please choose action in below menu");
			}
			if (exit) {
				break;
			}
			showMenu();
		}
	}

	public static void showMenu() {
		System.out.println("--------Menu--------");
		System.out.println("1. Add student");
		System.out.println("2. Update student by id");
		System.out.println("3. Delete student by id");
		System.out.println("4. Sort student by gpa");
		System.out.println("5. Sort student by name");
		System.out.println("6. Show student");
		System.out.println("0. Exit");
		System.out.println("--------------------");
		System.out.print("Please choose: ");
	}

}
