//Liet ke so nguyen to nho hon n
package baitap;

import java.util.Scanner;

public class Bai3 {

	public static void main(String[] args) {
		System.out.print("Moi nhap n: ");
		int n = nhap();
		System.out.print("cac snt ");
		lietke(n);
	}

	public static int nhap() {
		Scanner sc = new Scanner(System.in);
		boolean check = false;
		int n = 0;
		while (!check) {
			System.out.print(" ");
			try {
				n = sc.nextInt();
				check = true;
			} catch (Exception e) {
				System.out.print("Ban phai nhap so, moi nhap lai...");
				sc.nextLine();
			}
		}
		sc.close();
		return n;
	}

	public static int ktSNT(int n) {
		int kt = 1;
		if (n < 2) {
			kt = 0;
		} else if (n == 2) {
			kt = 1;
		} else if (n % 2 == 0) {
			kt = 0;
		} else {
			for (int i = 3; i < n; i += 2) {
				if (n % i == 0) {
					kt = 0;
					break;
				}
			}
		}
		return kt;
	}

	public static void lietke(int n) {
		for (int i = 2; i <= n; i++) {
			if (ktSNT(i) == 1) {
				System.out.print(i + " ");
			}
		}
	}
}
