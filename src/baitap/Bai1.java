//Tim UCLN, BCNN cua 2 so a va b
package baitap;

import java.util.Scanner;

public class Bai1 {

	public static void main(String[] args) {
		
		System.out.print("Nhap a: ");
		int a = nhap();
		System.out.print("Nhap b: ");
		int b = nhap();
		
		System.out.println("UCLN cua " + a + " va " + b + " la: " + UCLN(a,b));
		System.out.println("BCNN cua " + a + " va " + b + " la: " + (a*b)/UCLN(a,b));
	}
	
	public static int nhap() {
		Scanner sc = new Scanner(System.in);
		boolean check = false;
		int n = 0;
		while (!check) {
			System.out.print(" ");
			try {
				n = sc.nextInt();
				check = true;
			} catch (Exception e) {
				System.out.println("Ban phai nhap so! hay nhap lai...");
				sc.nextLine();
			}
		}
		return n;
	}
	
	public static int UCLN(int a, int b) {
		while(a != b) {
			if(a > b) {
				a = a -b;
			} else {
				b = b -a;
			}
		}
		return a;
	}
}
