//Tinh giai thua cua n
package baitap;

import java.util.Scanner;

public class Bai2 {

	public static void main(String[] args) {
		
		System.out.print("Nhap n: ");
		int n = nhap();
		
		
		System.out.println("Giai thua cua " + n + " la " + tinhGiaithua(n));
	}
	
	public static int nhap() {
		Scanner sc = new Scanner(System.in);
		boolean check = false;
		int n = 0;
		while (!check) {
			System.out.print(" ");
			try {
				n = sc.nextInt();
				check = true;
			} catch (Exception e) {
				System.out.println("Ban phai nhap so! hay nhap lai...");
				sc.nextLine();
			}
		}
		sc.close();
		return n;
	}
	
	public static long tinhGiaithua(int n) {
        long giai_thua = 1;
        if (n == 0 || n == 1) {
            return giai_thua;
        } else {
            for (int i = 2; i <= n; i++) {
                giai_thua *= i;
            }
            return giai_thua;
        }
    }

}
