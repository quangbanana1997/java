// Nhap mot mang so nguyen. In ra man hinh phan tu xuat hien trong mang dung 1 lan 
package baitap;

import java.util.Scanner;

public class Bai5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap so phan tu cua mang: ");
		int n = sc.nextInt();

		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			System.out.print("a[" + i + "] = ");
			arr[i] = sc.nextInt();
		}
		
		System.out.print("Cac phan tu trong day xuat hien 1 lan: ");
		for (int i = 0; i < n; i++) {
			if (countElement(arr, n, arr[i]) == 1) {
				System.out.print(" " + arr[i]);
			}
		}

		sc.close();
	}

	public static int countElement(int a[], int n, int i) {
		int count = 0;
		for (int j = 0; j < n; j++) {
			if (a[j] == i)
				count++;
		}
		return count;
	}

}
