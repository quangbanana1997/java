/*Viet chuong trinh nhap vao ma tran A co n dong, m cot, cac phan tu so nguyen lon hon 0 va nho hon 100.
Thuc hien cac chuc nang sau:
	a) Tim phan tu lon nhat voi chi so cua no (chi so dau tien tim duoc)
	b) Tim va in ra cac phan tu so nguyen to cua ma tran (cac phan tu khong nguyen to thi thay bang so 0)
	c) Sap xep tat ca cac cot cua ma tran theo thu tu tang dan va in ket qua ra man hinh
	d) Tim cot trong ma tran co nhieu so nguyen to nhat*/
package baitap;

import java.util.Scanner;

public class Bai7 {

	public static void main(String[] args) {

		System.out.print("Nhap so hang n: ");
		int n = nhap();
		System.out.print("Nhap so cot m:");
		int m = nhap();

		int[][] A = new int[n][m];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				System.out.print("Nhap phan tu thu A[" + i + "][" + j + "] = ");
				A[i][j] = nhap();
			}
		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (A[i][j] == findMaxMT(A, n, m)) {
					System.out.println(
							"Phan tu o hang " + i + " cot " + j + " dat max: A[" + i + "][" + j + "] = " + A[i][j]);
				}
			}
		}
	}

	public static int nhap() {
		Scanner sc = new Scanner(System.in);
		boolean check = false;
		int n = 0;
		while (!check) {
			System.out.print(" ");
			try {
				n = sc.nextInt();
				check = true;
			} catch (Exception e) {
				System.out.println("Ban phai nhap so! hay nhap lai...");
				sc.nextLine();
			}
		}
		return (n);
	}

	public static boolean checkSNT(int n) {
		boolean check = true;
		if (n < 2) {
			check = false;
		} else if (n == 2) {
			check = true;
		} else if (n % 2 == 0) {
			check = false;
		} else {
			for (int i = 3; i < n; i += 2) {
				if (n % i == 0) {
					check = false;
					break;
				}
			}
		}
		return check;
	}

	public static int findMaxMT(int[][] A, int n, int m) {
		int max = A[0][0];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (max < A[i][j]) {
					max = A[i][j];
				}
			}
		}
		return max;
	}

}
