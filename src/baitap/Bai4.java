//Tinh tong cac chu so cua mot so nguyen bat ky n
package baitap;

import java.util.Scanner;

public class Bai4 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap n: ");
		int n = sc.nextInt();
		int tong = 0;
		while (n > 0) {
			tong = n % 10 + tong;
			n = n / 10;
		}
		sc.close();
		System.out.println("Tong cac chu so cua n la: " + tong);
	}
}
