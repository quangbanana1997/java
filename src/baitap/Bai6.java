// Nhap mot mang so nguyen. In ra man hinh phan tu xuat hien trong mang dung 2 lan 
package baitap;

import java.util.Scanner;

public class Bai6 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap so phan tu cua mang: ");
		int n = sc.nextInt();

		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			System.out.print("a[" + i + "] = ");
			arr[i] = sc.nextInt();
		}
		
		System.out.print("Cac phan tu trong day xuat hien dung 2 lan la: ");
		for (int i = 0; i < n; i++) {
			if (countElement(arr, n, arr[i]) == 2 && countElement(arr, i, arr[i]) == 0) {
				System.out.print(" " + arr[i]);
			}
		}

		sc.close();
	}

	public static int countElement(int a[], int n, int i) {
		int count = 0;
		for (int j = 0; j < n; j++) {
			if (a[j] == i)
				count++;
		}
		return count;
	}

}
